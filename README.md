# SUBSEVEN

A not so imaginative name for a useful admin theme.

## BACKGROUND
This theme was spawned from the changes that were always applied to new Drupal
projects in our team and were not specific to those projects.

Subseven doesn't use the latest/fanciest toolkit around for CSS, it is just plain
old CSS with easy to write and read declarations that will make your daily
Drupal administration life easier.

## IMPORTANT
- Subseven hides a CTools visibility tab on blocks that can cause problems in
  the long run. Feel free to remove this from the css styles.  
  More info: https://www.drupal.org/project/ctools/issues/2857279

- Subseven displays the views preview area on the side on big screens.
  This will be removed from the theme once/if this gets added into core.  
  More info: https://www.drupal.org/project/drupal/issues/3185775

# CREDITS
Initial development: Bill Seremetis (bserem)  
Animations and UX: Aris Magrips (arismag)  
Sponsoring: zehnplus.ch
